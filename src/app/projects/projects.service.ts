import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  baseUrl:string="http://managementapp-env.5qjtgpkffk.us-east-1.elasticbeanstalk.com";
  constructor(private httpClient: HttpClient) { }

  getProjects(){

    return this.httpClient.get(this.baseUrl + '/projects' + '/all');
  }
}
